package com.aoc.model;


import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

    @Data
    @Entity
    public class Message {

        @Id
        @GeneratedValue
        private Integer id;
        private String message;

        private Message() {
        }

        public Message(String mess) {
            this.message = mess;
        }

    }

